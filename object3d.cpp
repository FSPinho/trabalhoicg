#include "object3d.h"

Object3D::Object3D(GLdouble x, GLdouble y, GLdouble z) {
    this->xt = x;
    this->yt = y;
    this->zt = z;

    this->xs = 1.0;
    this->ys = 1.0;
    this->zs = 1.0;

    this->xr = 0.0;
    this->yr = 0.0;
    this->zr = 0.0;

    setSelected(false);
}

Object3D::~Object3D() {

}

void Object3D::displayCube(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth) {
    glPushMatrix();

    glTranslatef(x, y, z);
    glScaled(width/1.0, height/1.0, depth/1.0);
    glutSolidCube(1.0);

    glPopMatrix();
}

void Object3D::displayCylinder(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth) {
    glPushMatrix(); // 1

    glRotatef(90, -1, 0, 0);
    glTranslatef(x, y, z);
    glScaled(width/1.0, depth/1.0, height/1.0);

    GLUquadricObj *quad;
    quad = gluNewQuadric();
    gluCylinder(quad, 0.5, 0.5, 1, 30, 1);
    glPushMatrix(); //2
    glTranslatef(0.0f, 0.0f, 1);
    gluDisk(quad, 0.0f, 0.5, 30, 1);
    glPopMatrix(); //2
    glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
    gluDisk(quad, 0.0f, 0.5, 30, 1);
    gluDeleteQuadric(quad);
    glPopMatrix(); //1
}

void Object3D::displaySphere(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth) {
    glPushMatrix();

    glTranslatef(x, y, z);
    glScaled(width/1.0, height/1.0, depth/1.0);
    glutSolidSphere(1.0, 32, 32);

    glPopMatrix();
}

bool Object3D::getSelected() const {
    return selected;
}

void Object3D::setSelected(bool value) {
    selected = value;
}

void Object3D::translate(GLdouble x, GLdouble y, GLdouble z) {
    this->xt += x;
    this->yt += y;
    this->zt += z;
}

void Object3D::rotate(GLdouble angle, GLdouble x, GLdouble y, GLdouble z) {
    this->xr += angle*x;
    this->yr += angle*y;
    this->zr += angle*z;
}

void Object3D::scale(GLdouble x, GLdouble y, GLdouble z) {
    this->xs *= x;
    this->ys *= y;
    this->zs *= z;
}

void Object3D::scale(GLdouble s) {
    scale(s, s, s);
}

void Object3D::display() {
    glPushMatrix();

    glTranslated(xt, yt, zt);
    glRotated(xr, 1, 0, 0);
    glRotated(yr, 0, 1, 0);
    glRotated(zr, 0, 0, 1);
    glScaled(xs, ys, zs);

    if(getSelected())
        onDisplaySelected();
    else
        onDisplay();

    glPopMatrix();
}

void Object3D::displaySelected() {
    glPushMatrix();

    glTranslated(xt, yt, zt);
    glRotated(xr, 1, 0, 0);
    glRotated(yr, 0, 1, 0);
    glRotated(zr, 0, 0, 1);
    glScaled(xs, ys, zs);

    onDisplaySelected();

    glPopMatrix();
}

void Object3D::displayShadow() {
    glPushMatrix();

    glTranslated(xt, yt, zt);
    glRotated(xr, 1, 0, 0);
    glRotated(yr, 0, 1, 0);
    glRotated(zr, 0, 0, 1);
    glScaled(xs, ys, zs);

    onDisplayShadow();

    glPopMatrix();
}


