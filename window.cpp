#include "window.h"

Window* currentInstance;

static void displayCallBack() {
    currentInstance->display();
}

static void reshapeCallBack(GLsizei w, GLsizei h) {
    currentInstance->reshape(w, h);
}

static void processKeysCallBack(int key, int x, int y) {
    currentInstance->processKeys(key, x, y);
}

static void processKeysCharCallBack(unsigned char key, int x, int y) {
    currentInstance->processKeysChar(key, x, y);
}

static void processMouseCallBack(int button, int state, int x, int y) {
    currentInstance->processMouse(button, state, x, y);
}

static void processMouseMotionCallBack(int x, int y) {
    currentInstance->processMuseMotion(x, y);
}

Window::Window(int width, int height, string title) {
    currentInstance = this;

    viewAngleX = 0.0f;
    viewAngleY = 0.0f;

    flagMouseDownRotate = false;
    flagMouseDownMoved = false;
    mouseX = mouseY = 0;
    camEX = 0, camEY = 0, camEZ = 20;
    camCX = 0, camCY = 0, camCZ = 0;
    camUX = 0, camUY = 1, camUZ = 0;

    lightX = 50;
    lightY = 50;
    lightZ = 50;

    moovingEnabled = false;
    rotateEnabled = false;
    scaleEnabled = false;
    keyXEnabled = false;
    keyYEnabled = false;
    keyZEnabled = false;

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(0, 0);
    glutCreateWindow(title.c_str());

    glutDisplayFunc(displayCallBack);
    glutReshapeFunc(reshapeCallBack);
    glutSpecialFunc(processKeysCallBack);
    glutKeyboardFunc(processKeysCharCallBack);
    glutMouseFunc(processMouseCallBack);
    glutMotionFunc(processMouseMotionCallBack);

    init();

}

Window::~Window() { }

void Window::addObject(Object3D* obj) {
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->setSelected(false);
    this->objects.push_back(obj);
    //obj->setSelected(true);
}

void Window::addObject(vector<Object3D *> objs) {
    for(vector<Object3D*>::iterator i = objs.begin(); i != objs.end(); i++) {
        (*i)->setSelected(false);
        addObject(*i);
    }
}

void Window::display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    /* Display objects */
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->display();

    GLdouble shadowMatrix[16] = {
        -lightY, -lightX, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0,
        0.0, lightZ, -lightY, 0.0,
        0.0, -1.0, 0.0, lightY
    };

    glMultTransposeMatrixd(shadowMatrix);

    glDisable(GL_LIGHTING);
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->displayShadow();
    glEnable(GL_LIGHTING);

    glFlush();
    glutSwapBuffers();
}

void Window::reshape(GLsizei w, GLsizei h) {
    // Para previnir uma divisão por zero
    if ( h == 0 ) h = 1;

    // Especifica o tamanho da viewport
    glViewport(0, 0, w, h);

    // Calcula a correção de aspecto
    fAspect = (GLfloat)w/(GLfloat)h;

    viewParams();
}

void Window::processKeys(int key, int x, int y) {
    switch(key) {
    case GLUT_KEY_UP:
        nextObject(true);
        break;
    case GLUT_KEY_DOWN:
        nextObject(false);
        break;
    case GLUT_KEY_RIGHT:
        nextObject(true);
        break;
    case GLUT_KEY_LEFT:
        nextObject(false);
        break;
    }

    viewParams();
    glutPostRedisplay();
}

void Window::processKeysChar(unsigned char key, int x, int y) {
    switch(key) {
    case 'x':
        cout << "x" << endl;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled= false;
        break;
    case 'y':
        cout << "y" << endl;
        keyYEnabled = true;
        keyXEnabled = keyZEnabled= false;
        break;
    case 'z':
        cout << "z" << endl;
        keyZEnabled = true;
        keyXEnabled = keyYEnabled= false;
        break;
    case 'g':
        cout << "Translate" << endl;
        moovingEnabled = true;
        rotateEnabled = false;
        scaleEnabled = false;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case 'r':
        cout << "Rotate" << endl;
        moovingEnabled = false;
        rotateEnabled = true;
        scaleEnabled = false;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case 's':
        cout << "Scale" << endl;
        moovingEnabled = false;
        rotateEnabled = false;
        scaleEnabled = true;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case 27:
        cout << "View" << endl;
        moovingEnabled = false;
        scaleEnabled = false;
        rotateEnabled = false;
        break;

    }



    viewParams();
    glutPostRedisplay();
}

void Window::processMouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) {
            flagMouseDownRotate = true;
            mouseX = x; mouseY = y;
        } else {
            flagMouseDownRotate = false;
        }
    }
    if (button == GLUT_RIGHT_BUTTON) {
        if (state == GLUT_DOWN) {
            flagMouseDownMoved = true;
            mouseX = x; mouseY = y;
        } else {
            flagMouseDownMoved = false;
        }
    }

    switch(button) {
        case 3:
            camEZ -= camEZ > 10? 1: camEZ > 1? .1: .01;
            break;
        case 4:
            camEZ += camEZ > 10? 1: camEZ > 1? .1: .01;
            break;
        case 5:
            break;
        case 6:
            break;
    }

    viewParams();
    glutPostRedisplay();
    cout << camEZ <<  endl;
}

void Window::processMuseMotion(int x, int y) {

    Object3D* object = NULL;
    for(int i = 0; i < objects.size(); i++) {
        if(objects.at(i)->getSelected()) {
            object = objects.at(i);
        }
    }

    if(moovingEnabled) {
        if(keyXEnabled && object != NULL) {
            object->translate(-(mouseX - x)*0.01, 0, 0);

        } else if(keyYEnabled && object != NULL) {
            object->translate(0, (mouseY - y)*0.01, 0);

        } else if(keyZEnabled && object != NULL) {
            object->translate(0, 0, -(mouseY - y)*0.01);

        }
    } else if(rotateEnabled) {
        if(keyXEnabled && object != NULL) {
            object->rotate(-(mouseY - y)*0.5, 1, 0, 0);

        } else if(keyYEnabled && object != NULL) {
            object->rotate(-(mouseX - x)*0.5, 0, 1, 0);

        } else if(keyZEnabled && object != NULL) {
            object->rotate((mouseY - y)*0.5, 0, 0, 1);

        }
    } else if(scaleEnabled) {
        if(keyXEnabled && object != NULL) {
            object->scale((mouseY - y)*0.01);

        }
    } else if(flagMouseDownRotate) {
        viewAngleY -= (mouseX - x)*0.8;
        viewAngleX -= (mouseY - y)*0.8;

    } else if(flagMouseDownMoved) {
        camCX += (mouseX - x)*0.01;
        camEX += (mouseX - x)*0.01;
        //camUX += (mouseX - x)*0.01;

        camCY -= (mouseY - y)*0.01;
        camEY -= (mouseY - y)*0.01;
        //camUY += (mouseY - y)*0.01;

    }

    mouseX = x;
    mouseY = y;

    viewParams();
    glutPostRedisplay();

}

int Window::exec() {
    glutMainLoop();

    return 0;
}

void Window::init() {
    // Especifica que a cor de fundo da janela será branca
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

    // Habilita a definição da cor do material a partir da cor corrente
    glEnable(GL_COLOR_MATERIAL);
    //Habilita o uso de iluminação
    glEnable(GL_LIGHTING);
    //Habilita a normalização
    glEnable(GL_NORMALIZE);

    // Habilita o modelo de colorização de Gouraud
    glShadeModel(GL_SMOOTH);

    // Habilita o depth-buffering
    glEnable(GL_DEPTH_TEST);

    // Habilita a luz de número 0
    glEnable(GL_LIGHT0);


    GLfloat luzAmbiente[4]={0.2 ,0.2 ,0.2, 1.0};
    GLfloat luzDifusa[4]={0.7, 0.7, 0.7, 1.0};	     // "cor"
    GLfloat luzEspecular[4]={1.0, 1.0, 1.0, 1.0};// "brilho"
    GLfloat posicaoLuz[4]={lightX, lightY, lightZ, 0.0};

    // Capacidade de brilho do material
    GLfloat especularidade[4]={1.0,1.0,1.0,1.0};
    GLint especMaterial = 100;


    // Define a refletância do material
    glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
    // Define a concentração do brilho
    glMateriali(GL_FRONT,GL_SHININESS, especMaterial);

    // Ativa o uso da luz ambiente
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

    // Define os parâmetros da luz de número 0
    glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa );
    glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular );
    glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );

    angle=45;

    viewParams();
    glutPostRedisplay();
}

void Window::viewParams() {
    // Especifica sistema de coordenadas de projeção
    glMatrixMode(GL_PROJECTION);
    // Inicializa sistema de coordenadas de projeção
    glLoadIdentity();

    // Especifica a projeção perspectiva
    gluPerspective(angle,fAspect,0.4,500);

    // Especifica sistema de coordenadas do modelo
    glMatrixMode(GL_MODELVIEW);
    // Inicializa sistema de coordenadas do modelo
    glLoadIdentity();

    // Especifica posição do observador e do alvo
    gluLookAt(camEX,camEY,camEZ, camCX,camCY,camCZ, camUX,camUY,camUZ);

    glRotatef(viewAngleX, 1, 0, 0);
    glRotatef(viewAngleY, 0, 1, 0);
}

void Window::nextObject(bool next) {

    if(objects.size() > 0) {
        int size = objects.size();
        for(int i = 0; i < size; i++) {
            Object3D* object = objects.at(i);
            if(object->getSelected()) {

                object->setSelected(false);

                if(next)
                    object = objects.at((i + 1) % size);
                else
                    object = objects.at((i + (size - 1)) % size);

                object->setSelected(true);

                break;
            }
        }
    }

}

