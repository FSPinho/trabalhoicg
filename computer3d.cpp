#include "computer3d.h"

Computer3D::Computer3D(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {}

Computer3D::~Computer3D() { }

void Computer3D::onDisplay() {

    glColor3d(.9, .9, .9);
    Object3D::displayCylinder(-1, 0, 0, 1.5, .2, 1);

    glPushMatrix();
    glRotatef(10, 1, 0, 0);
    Object3D::displayCylinder(-1, 0, 0, .4, 1, .2);
    glPopMatrix();

    glPushMatrix();
    glRotatef(10, -1, 0, 0);
    Object3D::displayCube(-1, 2, .4, 1.6*3, .9*3, .2);

    glColor3d(0, 0, 0);
    glScalef(.95, .9, .9);
    Object3D::displayCube(-1, 2.2, .5, 1.6*3, .9*3, .2);
    glPopMatrix();

    glColor3d(.9, .9, .9);

    Object3D::displayCube(2.5, 3.5/2, 0, 1.7, 3.5, 2.5);
    Object3D::displayCube(-1, 0, 2, 4, .2, 2);

}

void Computer3D::onDisplaySelected() {
    glColor3d(1, 0, 0);
    Object3D::displayCylinder(-1, 0, 0, 1.5, .2, 1);

    glPushMatrix();
    glRotatef(10, 1, 0, 0);
    Object3D::displayCylinder(-1, 0, 0, .4, 1, .2);
    glPopMatrix();

    glPushMatrix();
    glRotatef(10, -1, 0, 0);
    Object3D::displayCube(-1, 2, .4, 1.6*3, .9*3, .2);

    glScalef(.95, .9, .9);
    Object3D::displayCube(-1, 2.2, .5, 1.6*3, .9*3, .2);
    glPopMatrix();

    Object3D::displayCube(2.5, 3.5/2, 0, 1.7, 3.5, 2.5);
    Object3D::displayCube(-1, 0, 2, 4, .2, 2);
}

void Computer3D::onDisplayShadow() {
    glColor3d(0, 0, 0);
    Object3D::displayCylinder(-1, 0, 0, 1.5, .2, 1);

    glPushMatrix();
    glRotatef(10, 1, 0, 0);
    Object3D::displayCylinder(-1, 0, 0, .4, 1, .2);
    glPopMatrix();

    glPushMatrix();
    glRotatef(10, -1, 0, 0);
    Object3D::displayCube(-1, 2, .4, 1.6*3, .9*3, .2);

    glScalef(.95, .9, .9);
    Object3D::displayCube(-1, 2.2, .5, 1.6*3, .9*3, .2);
    glPopMatrix();

    Object3D::displayCube(2.5, 3.5/2, 0, 1.7, 3.5, 2.5);
    Object3D::displayCube(-1, 0, 2, 4, .2, 2);
}

