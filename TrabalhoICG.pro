TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    window.cpp \
    object3d.cpp \
    computer3d.cpp \
    floor3d.cpp \
    table3d.cpp \
    chair3d.cpp \
    composecct.cpp \
    blackboard.cpp \
    fract3d.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    window.h \
    object3d.h \
    computer3d.h \
    floor3d.h \
    table3d.h \
    chair3d.h \
    composecct.h \
    blackboard.h \
    fract3d.h

LIBS += -lGL -lGLU -lglut
