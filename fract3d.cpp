#include "fract3d.h"

Fract3D::Fract3D(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {
    vAngle = 0;
}

void Fract3D::onDisplay() {
    vAngle += .1;
    glColor3f(0, .5, 0);
    glTranslated(0, 10, 0);
    drawFract2(12);
}

void Fract3D::onDisplaySelected() {

}

void Fract3D::onDisplayShadow() {

}

void Fract3D::drawFract(int iterations) {
    if(iterations == 0) return;

    Object3D::displayCube(0, 1, 0, 1, 1, 1);

    glPushMatrix();

    glTranslatef(-.25/2.0, 1.5, 0);
    glRotatef(45, 0, 0, 1);
    glScalef(.5, .5, .5);
    drawFract(iterations-1);

    glPopMatrix();

    glPushMatrix();

    glTranslatef(.25/2.0, 1.5, 0);
    glRotatef(-45, 0, 0, 1);
    glScalef(.5, .5, .5);
    drawFract(iterations-1);

    glPopMatrix();

}

void Fract3D::drawFract2(int iterations) {
    if(iterations == 0) return;

    const GLdouble scale = .7;
    const GLdouble height = 8;
    const GLdouble angle = vAngle;

    Object3D::displayCube(0, 0, 0, 1, height, 1);


        glPushMatrix();

        glRotated(90, 0, 0, 1);
        glRotated(angle, 0, 0, 1);
        glTranslated(height/2 + .5*scale, 0, 0);
        glScaled(scale, scale, 1);

        drawFract2(iterations-1);

        glPopMatrix();


        glPushMatrix();

        glRotated(90, 0, 0, 1);
        glRotated(angle, 0, 0, 1);
        glTranslated(-(height/2 + .5*scale), 0, 0);
        glScaled(scale, scale, 1);

        drawFract2(iterations-1);

        glPopMatrix();

}
