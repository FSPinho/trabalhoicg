#include "composecct.h"

ComposeCCT::ComposeCCT(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {
    Object3D* comp = new Computer3D(0, 2.12, -.5);
    comp->scale(.4);
    objects.push_back(comp);

    objects.push_back(new Table3D(0, 0, 0));

    Object3D* chair = new Chair3D(0, 0, 2);
    chair->scale(.7);
    objects.push_back(chair);
}

ComposeCCT::~ComposeCCT() { }

void ComposeCCT::onDisplay() {
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->display();
}

void ComposeCCT::onDisplaySelected() {
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->displaySelected();
}

void ComposeCCT::onDisplayShadow() {
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->displayShadow();
}

