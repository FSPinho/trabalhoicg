#ifndef COMPOSECCT_H
#define COMPOSECCT_H

#include "object3d.h"
#include "chair3d.h"
#include "computer3d.h"
#include "table3d.h"
#include <vector>

class ComposeCCT : public Object3D {
public:
    ComposeCCT(GLdouble x, GLdouble y, GLdouble z);
    ~ComposeCCT();

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

private:
    vector<Object3D*> objects;

};

#endif // COMPOSECCT_H
