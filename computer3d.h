#ifndef COMPUTER3D_H
#define COMPUTER3D_H

#include <iostream>
#include "object3d.h"

using namespace std;

class Computer3D : public Object3D {
public:
    Computer3D(GLdouble x, GLdouble y, GLdouble z);
    ~Computer3D();

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

};

#endif // COMPUTER3D_H
