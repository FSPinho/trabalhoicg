#include "table3d.h"

Table3D::Table3D(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) { }

Table3D::~Table3D() {

}

void Table3D::onDisplay() {
    glColor3d(.9, .5, 0);
    Object3D::displayCube(-1, 1, 1, .2, 2, .2);
    Object3D::displayCube(-1, 1, -1, .2, 2, .2);
    Object3D::displayCube(1, 1, -1, .2, 2, .2);
    Object3D::displayCube(1, 1, 1, .2, 2, .2);

    Object3D::displayCube(0, 2, 0, 3, .2, 3);
}

void Table3D::onDisplaySelected() {
    glColor3d(1, 0, 0);
    Object3D::displayCube(-1, 1, 1, .2, 2, .2);
    Object3D::displayCube(-1, 1, -1, .2, 2, .2);
    Object3D::displayCube(1, 1, -1, .2, 2, .2);
    Object3D::displayCube(1, 1, 1, .2, 2, .2);

    Object3D::displayCube(0, 2, 0, 3, .2, 3);
}

void Table3D::onDisplayShadow() {
    glColor3d(0, 0, 0);
    Object3D::displayCube(-1, 1, 1, .2, 2, .2);
    Object3D::displayCube(-1, 1, -1, .2, 2, .2);
    Object3D::displayCube(1, 1, -1, .2, 2, .2);
    Object3D::displayCube(1, 1, 1, .2, 2, .2);

    Object3D::displayCube(0, 2, 0, 3, .2, 3);
}

