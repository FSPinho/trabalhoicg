#ifndef FRACT3D_H
#define FRACT3D_H

#include "object3d.h"

class Fract3D : public Object3D {
public:
    Fract3D(GLdouble x, GLdouble y, GLdouble z);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

private:

    float vAngle;

    void drawFract(int iterations);
    void drawFract2(int iterations);

};

#endif // FRACT3D_H
