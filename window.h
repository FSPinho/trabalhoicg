#ifndef WINDOW_H
#define WINDOW_H

#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <string>
#include <vector>
#include "object3d.h"

using namespace std;

class Window {
public:
    Window(int width, int height, string title);
    ~Window();

    void addObject(Object3D* obj);
    void addObject(vector<Object3D*> objs);

    void display();
    void reshape(GLsizei w, GLsizei h);
    void processKeys(int key, int x, int y);
    void processKeysChar(unsigned char key, int x, int y);
    void processMouse(int button, int state, int x, int y);
    void processMuseMotion(int x, int y);

    int exec();

private:
    vector<Object3D*> objects;

    GLfloat angle, fAspect;
    GLfloat viewAngleX;
    GLfloat viewAngleY;

    bool flagMouseDownRotate;
    bool flagMouseDownMoved;
    float mouseX, mouseY;
    float camEX, camEY, camEZ;
    float camCX, camCY, camCZ;
    float camUX, camUY, camUZ;

    GLfloat lightX, lightY, lightZ;

    bool moovingEnabled;
    bool rotateEnabled;
    bool scaleEnabled;
    bool keyXEnabled;
    bool keyYEnabled;
    bool keyZEnabled;

    void init();
    void viewParams();
    void nextObject(bool);

};

//Iure

#endif // WINDOW_H
