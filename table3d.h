#ifndef TABLE3D_H
#define TABLE3D_H

#include "object3d.h"

class Table3D : public Object3D {
public:

    Table3D(GLdouble x, GLdouble y, GLdouble z);
    ~Table3D();

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();
};

#endif // TABLE3D_H
