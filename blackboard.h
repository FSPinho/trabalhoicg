#ifndef BLACKBOARD_H
#define BLACKBOARD_H

#include <object3d.h>

class BlackBoard : public Object3D {
public:
    BlackBoard(GLdouble x, GLdouble y, GLdouble z);
    ~BlackBoard();

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();
};

#endif // BLACKBOARD_H
