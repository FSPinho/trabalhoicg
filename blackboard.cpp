#include "blackboard.h"

BlackBoard::BlackBoard(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {

}

BlackBoard::~BlackBoard() { }

void BlackBoard::onDisplay() {
    Object3D::displayCube(0, 4, -15, 14, 4, .2);
}

void BlackBoard::onDisplaySelected() {
}

void BlackBoard::onDisplayShadow() {
}

