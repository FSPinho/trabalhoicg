#ifndef CHAIR3D_H
#define CHAIR3D_H

#include "object3d.h"

class Chair3D : public Object3D {

public:
    Chair3D(GLdouble x, GLdouble y, GLdouble z);
    ~Chair3D();

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

};

#endif // CHAIR3D_H
