#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>

using namespace std;

class Object3D {
public:
    Object3D(GLdouble x, GLdouble y, GLdouble z);
    ~Object3D();

    static void displayCube(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth);
    static void displayCylinder(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth);
    static void displaySphere(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth);

    bool getSelected() const;
    void setSelected(bool value);

    void translate(GLdouble x, GLdouble y, GLdouble z);
    void rotate(GLdouble angle, GLdouble x, GLdouble y, GLdouble z);
    void scale(GLdouble x, GLdouble y, GLdouble z);
    void scale(GLdouble scale);

    void display();
    void displaySelected();
    void displayShadow();
    virtual void onDisplay() = 0;
    virtual void onDisplaySelected() = 0;
    virtual void onDisplayShadow() = 0;

private:
    GLdouble xt, yt, zt;
    GLdouble xr, yr, zr;
    GLdouble xs, ys, zs;

    bool selected;

};

#endif // OBJECT3D_H
