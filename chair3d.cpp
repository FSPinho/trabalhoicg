#include "chair3d.h"

Chair3D::Chair3D(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) { }

Chair3D::~Chair3D() { }

void Chair3D::onDisplay() {
    glColor3f(.3, .3, .3);

    Object3D::displayCube(0, 2, 0, 2, .2, 2);
    Object3D::displayCylinder(0, 0, 0, .2, 2, .2);
    Object3D::displayCylinder(0, -1, 2, .2, 2, .2);
    Object3D::displayCube(0, 3.5, 1, 2, 2, .2);

    glRotatef(45, 0, 1, 0);
    Object3D::displayCube(0, .1, 0, 2, .2, .2);
    Object3D::displaySphere(1, 0, 0, .2, .2, .2);
    Object3D::displaySphere(-1, 0, 0, .2, .2, .2);
    glRotatef(90, 0, 1, 0);
    Object3D::displaySphere(1, 0, 0, .2, .2, .2);
    Object3D::displaySphere(-1, 0, 0, .2, .2, .2);
    Object3D::displayCube(0, .1, 0, 2, .2, .2);
}

void Chair3D::onDisplaySelected() {
    glColor3f(1, 0, 0);

    Object3D::displayCube(0, 2, 0, 2, .2, 2);
    Object3D::displayCylinder(0, 0, 0, .2, 2, .2);
    Object3D::displayCylinder(0, -1, 2, .2, 2, .2);
    Object3D::displayCube(0, 3.5, 1, 2, 2, .2);

    glRotatef(45, 0, 1, 0);
    Object3D::displayCube(0, .1, 0, 2, .2, .2);
    Object3D::displaySphere(1, 0, 0, .2, .2, .2);
    Object3D::displaySphere(-1, 0, 0, .2, .2, .2);
    glRotatef(90, 0, 1, 0);
    Object3D::displaySphere(1, 0, 0, .2, .2, .2);
    Object3D::displaySphere(-1, 0, 0, .2, .2, .2);
    Object3D::displayCube(0, .1, 0, 2, .2, .2);
}

void Chair3D::onDisplayShadow() {
    glColor3f(0, 0, 0);

    Object3D::displayCube(0, 2, 0, 2, .2, 2);
    Object3D::displayCylinder(0, 0, 0, .2, 2, .2);
    Object3D::displayCylinder(0, -1, 2, .2, 2, .2);
    Object3D::displayCube(0, 3.5, 1, 2, 2, .2);

    glRotatef(45, 0, 1, 0);
    Object3D::displayCube(0, .1, 0, 2, .2, .2);
    Object3D::displaySphere(1, 0, 0, .2, .2, .2);
    Object3D::displaySphere(-1, 0, 0, .2, .2, .2);
    glRotatef(90, 0, 1, 0);
    Object3D::displaySphere(1, 0, 0, .2, .2, .2);
    Object3D::displaySphere(-1, 0, 0, .2, .2, .2);
    Object3D::displayCube(0, .1, 0, 2, .2, .2);
}

