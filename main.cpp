#include <iostream>
#include "window.h"
#include "floor3d.h"
#include "composecct.h"
#include "blackboard.h"
#include "fract3d.h"

using namespace std;

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);

    Window w(640, 480, "ICG");
    w.addObject(new Floor3D(0, -0.1, 0));

    w.addObject(new Fract3D(0, 0, 0));

    return w.exec();
}

